#!/usr/bin/env bash

set -x
set -e 

stow --ignore="stow\.sh" -t $HOME .

# Go Variables
GOPATH=$HOME/go

# Go binaries and tooling
PATH=$PATH:/usr/local/go/bin

# Rust tooling
PATH=$PATH:$HOME/.rustup/bin
PATH=$PATH:$HOME/.cargo/bin